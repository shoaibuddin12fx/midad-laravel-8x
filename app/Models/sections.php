<?php

use Illuminate\Database\Eloquent\Model;

class sections extends Model {
	public $timestamps = false;
	protected $table = 'sections';
}

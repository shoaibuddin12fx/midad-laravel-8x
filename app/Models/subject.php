<?php

use Illuminate\Database\Eloquent\Model;

class subject extends Model {
	public $timestamps = false;
	protected $table = 'subject';
}

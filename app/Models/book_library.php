<?php

use Illuminate\Database\Eloquent\Model;

class book_library extends Model {
	public $timestamps = false;
	protected $table = 'book_library';
}

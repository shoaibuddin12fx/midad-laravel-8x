<?php

use Illuminate\Database\Eloquent\Model;

class static_pages extends Model {
	public $timestamps = false;
	protected $table = 'static_pages';
}

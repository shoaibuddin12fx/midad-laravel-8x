<?php

use Illuminate\Database\Eloquent\Model;

class payroll_salary_base extends Model {
	public $timestamps = false;
	protected $table = 'payroll_salary_base';
}

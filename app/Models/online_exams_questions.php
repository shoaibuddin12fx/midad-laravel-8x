<?php

use Illuminate\Database\Eloquent\Model;

class online_exams_questions extends Model {
	public $timestamps = false;
	protected $table = "online_exams_questions";
}

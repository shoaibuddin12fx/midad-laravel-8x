<?php

use Illuminate\Database\Eloquent\Model;

class assignments extends Model {
	public $timestamps = false;
	protected $table = 'assignments';
}

<?php

use Illuminate\Database\Eloquent\Model;

class online_exams_grades extends Model {
	public $timestamps = false;
	protected $table = 'online_exams_grades';
}

<?php

use Illuminate\Database\Eloquent\Model;

class mm_uploads extends Model {
	public $timestamps = false;
	protected $table = 'mm_uploads';
}

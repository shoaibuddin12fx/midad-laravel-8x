<?php

use Illuminate\Database\Eloquent\Model;

class newsboard extends Model {
	public $timestamps = false;
	protected $table = 'newsboard';
}

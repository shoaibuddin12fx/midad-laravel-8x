<?php

use Illuminate\Database\Eloquent\Model;


class assignments_answers extends Model {
	public $timestamps = false;
	protected $table = 'assignments_answers';
}

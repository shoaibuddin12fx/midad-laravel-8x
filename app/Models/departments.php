<?php

use Illuminate\Database\Eloquent\Model;

class departments extends Model {
	public $timestamps = false;
	protected $table = "departments";
}

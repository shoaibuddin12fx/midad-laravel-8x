<?php

use Illuminate\Database\Eloquent\Model;

class frontend_pages extends Model {
	public $timestamps = false;
	protected $table = 'frontend_pages';
}

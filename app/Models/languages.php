<?php

use Illuminate\Database\Eloquent\Model;

class languages extends Model {
	public $timestamps = false;
	protected $table = 'languages';
}

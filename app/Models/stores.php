<?php

use Illuminate\Database\Eloquent\Model;

class stores extends Model {
	public $timestamps = false;
	protected $table = "stores";
}

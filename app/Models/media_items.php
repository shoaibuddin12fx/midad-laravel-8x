<?php

use Illuminate\Database\Eloquent\Model;

class media_items extends Model {
	public $timestamps = false;
	protected $table = 'media_items';
}

<?php

use Illuminate\Database\Eloquent\Model;

class mailsms_templates extends Model {
	public $timestamps = false;
	protected $table = 'mailsms_templates';
}

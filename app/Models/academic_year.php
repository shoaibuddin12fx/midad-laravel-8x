<?php

use Illuminate\Database\Eloquent\Model;

class academic_year extends Model {
	public $timestamps = false;
	protected $table = 'academic_year';
}

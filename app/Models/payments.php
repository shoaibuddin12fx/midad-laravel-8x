<?php

use Illuminate\Database\Eloquent\Model;

class payments extends Model {
	public $timestamps = false;
	protected $table = 'payments';
}

<?php

use Illuminate\Database\Eloquent\Model;

class online_exams extends Model {
	public $timestamps = false;
	protected $table = 'online_exams';
}

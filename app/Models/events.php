<?php

use Illuminate\Database\Eloquent\Model;

class events extends Model {
	public $timestamps = false;
	protected $table = 'events';
}

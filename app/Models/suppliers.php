<?php

use Illuminate\Database\Eloquent\Model;

class suppliers extends Model {
	public $timestamps = false;
	protected $table = "suppliers";
}

<?php

use Illuminate\Database\Eloquent\Model;

class fee_discount extends Model {
	public $timestamps = false;
	protected $table = 'fee_discount';
}
